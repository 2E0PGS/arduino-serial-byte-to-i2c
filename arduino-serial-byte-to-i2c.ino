/*
Arduino serial byte to I2C

Copyright (C) <2016-2020>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Wire.h>

void setup() {
  Wire.begin(); //inital wire bus
  Serial.begin(9600); //inital serial communication
  
  digitalWrite(13, LOW);
}

byte byte_to_send;

void loop() {
  while (Serial.available() == 0);
  
  byte_to_send = Serial.read() - '0';
  
  Serial.print("Going to send: ");
  Serial.println(byte_to_send);
  
  digitalWrite(13, HIGH);
  Wire.beginTransmission(7); // Transmit to device with ID 7.
  Wire.write(byte_to_send); // Sends one byte.
  Wire.endTransmission(); // Stop transmitting.
  delay(1000);
  digitalWrite(13, LOW);
}
